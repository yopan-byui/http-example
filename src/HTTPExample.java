import java.net.*;
import java.io.*;
import java.util.*;

public class HTTPExample {

    public static String getHttpContent(String string) {

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            StringBuilder stringBuilder = new StringBuilder();

            String line = null;

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return "Error";
    }

    public static Map getHttpHeaders(String string) {
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            return http.getHeaderFields();

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return null;
    }

    public static void main(String[] args) {
        String url = "http://www.google.com";
        System.out.println(HTTPExample.getHttpContent(url));

        Map<Integer, List<String>> headers = HTTPExample.getHttpHeaders(url);

        for (Map.Entry<Integer, List<String>> entry : headers.entrySet()) {
            System.out.println("Key= " + entry.getKey() + " | Value= " + entry.getValue());
        }
    }
}
